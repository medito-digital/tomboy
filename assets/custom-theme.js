/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./theme/scripts/api/klaviyo.js":
/*!**************************************!*\
  !*** ./theme/scripts/api/klaviyo.js ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "subscribe": function() { return /* binding */ subscribe; }
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { (0,_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

var API_ENDPOINT = "https://manage.kmail-lists.com/ajax/subscriptions/subscribe"; // eslint-disable-next-line import/prefer-default-export

var subscribe = function subscribe(listId, email) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var config = _objectSpread(_objectSpread({}, params), {}, {
    g: listId,
    email: email
  });

  var body = Object.keys(config).reduce(function (str, key) {
    str.append(key, config[key]);
    return str;
  }, new URLSearchParams());
  return fetch(API_ENDPOINT, {
    method: "POST",
    headers: {
      "Access-Control-Allow-Headers": "*",
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
    },
    body: body
  }).then(function (res) {
    return res.json();
  });
};

/***/ }),

/***/ "./theme/scripts/app.js":
/*!******************************!*\
  !*** ./theme/scripts/app.js ***!
  \******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var picoapp__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! picoapp */ "./node_modules/picoapp/dist/picoapp.es.js");
/* harmony import */ var _components_NotifyMe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/NotifyMe */ "./theme/scripts/components/NotifyMe.js");


var initialState = {};
var components = {
  NotifyMe: _components_NotifyMe__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ((0,picoapp__WEBPACK_IMPORTED_MODULE_0__.picoapp)(components, initialState));

/***/ }),

/***/ "./theme/scripts/components/NotifyMe.js":
/*!**********************************************!*\
  !*** ./theme/scripts/components/NotifyMe.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var picoapp__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! picoapp */ "./node_modules/picoapp/dist/picoapp.es.js");
/* harmony import */ var _api_klaviyo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/api/klaviyo */ "./theme/scripts/api/klaviyo.js");


var LIST_ID = "RiB63m";
/* harmony default export */ __webpack_exports__["default"] = ((0,picoapp__WEBPACK_IMPORTED_MODULE_0__.component)(function (node) {
  var form = node.querySelector("[data-notify-me-form]");
  var successMessage = form.querySelector("[data-notify-me-success]");
  var submitButton = form.querySelector("[data-notify-me-submit]");
  var name = form.querySelector("[name=name]");
  var email = form.querySelector("[name=email]");

  var handleResetFields = function handleResetFields() {
    name.value = "";
    email.value = "";
  };

  var onSubmitFormHandle = function onSubmitFormHandle() {
    if (!name.reportValidity() || !email.reportValidity()) {
      return;
    }

    var data = {
      $fields: ["Name", "Product"],
      Name: name.value,
      Product: window.location.href
    };
    submitButton.disabled = true;
    (0,_api_klaviyo__WEBPACK_IMPORTED_MODULE_1__.subscribe)(LIST_ID, email.value, data).then(function (response) {
      try {
        if (!(response !== null && response !== void 0 && response.success)) {
          throw new Error(response.message);
        }

        successMessage.style.display = "block";
        setTimeout(function () {
          successMessage.style.display = "none";
        }, 5000);
        console.log("### subscribe", response);
      } catch (error) {
        console.error(error);
      }
    }).finally(function () {
      handleResetFields();
      submitButton.disabled = false;
    });
  };

  submitButton.addEventListener("click", onSubmitFormHandle);
}));

/***/ }),

/***/ "./theme/styles/index.css":
/*!********************************!*\
  !*** ./theme/styles/index.css ***!
  \********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./node_modules/picoapp/dist/picoapp.es.js":
/*!*************************************************!*\
  !*** ./node_modules/picoapp/dist/picoapp.es.js ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "component": function() { return /* binding */ u; },
/* harmony export */   "picoapp": function() { return /* binding */ c; }
/* harmony export */ });
var n=function(n){if("object"!=typeof(t=n)||Array.isArray(t))throw"state should be an object";var t},t=function(n,t,r,e){return(o=n,o.reduce(function(n,t,r){return n.indexOf(t)>-1?n:n.concat(t)},[])).reduce(function(n,r){return n.concat(t[r]||[])},[]).map(function(n){return n(r,e)});var o};function r(r){void 0===r&&(r={});var e={};return{getState:function(){return Object.assign({},r)},hydrate:function(o){return n(o),Object.assign(r,o),function(){var n=["*"].concat(Object.keys(o));t(n,e,r)}},on:function(n,t){return(n=[].concat(n)).map(function(n){return e[n]=(e[n]||[]).concat(t)}),function(){return n.map(function(n){return e[n].splice(e[n].indexOf(t),1)})}},emit:function(o,u,c){var i=("*"===o?[]:["*"]).concat(o);(u="function"==typeof u?u(r):u)&&(n(u),Object.assign(r,u),i=i.concat(Object.keys(u))),t(i,e,r,c)}}}r();var e=function(n){return"object"==typeof n&&!Array.isArray(n)},o=function(n){return"function"==typeof n};function u(n){return function(t,r){var e=[];return{subs:e,unmount:n(t,Object.assign({},r,{on:function(n,t){var o=r.on(n,t);return e.push(o),o}})),node:t}}}function c(n,t,u){void 0===n&&(n={}),void 0===t&&(t={}),void 0===u&&(u=[]);var c=r(t),i=[];return{on:c.on,emit:c.emit,getState:function(){return c.getState()},add:function(t){if(!e(t))throw"components should be an object";Object.assign(n,t)},use:function(n){if(!o(n))throw"plugins should be a function";u.push(n)},hydrate:function(n){return c.hydrate(n)},mount:function(t){void 0===t&&(t="data-component"),t=[].concat(t);for(var r=0;r<t.length;r++){for(var a=t[r],f=[].slice.call(document.querySelectorAll("["+a+"]")),s=function(){for(var t=f.pop(),r=t.getAttribute(a).split(/\s/),s=0;s<r.length;s++){var v=n[r[s]];if(v){t.removeAttribute(a);try{var d=u.reduce(function(n,r){var o=r(t,c);return e(o)?Object.assign(n,o):n},{}),m=v(t,Object.assign({},d,c));o(m.unmount)&&i.push(m)}catch(n){console.error(n),c.emit("error",{error:n}),c.hydrate({error:void 0})}}}};f.length;)s();c.emit("mount")}},unmount:function(){for(var n=i.length-1;n>-1;n--){var t=i[n],r=t.subs;(0,t.unmount)(t.node),r.map(function(n){return n()}),i.splice(n,1)}c.emit("unmount")}}}
//# sourceMappingURL=picoapp.es.js.map


/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/defineProperty.js ***!
  \*******************************************************************/
/***/ (function(__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ _defineProperty; }
/* harmony export */ });
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
!function() {
/*!********************************!*\
  !*** ./theme/scripts/index.js ***!
  \********************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_index_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../styles/index.css */ "./theme/styles/index.css");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/app */ "./theme/scripts/app.js");



if (window.Shopify.designMode) {
  document.addEventListener("shopify:section:load", function () {
    _app__WEBPACK_IMPORTED_MODULE_1__["default"].unmount();
    _app__WEBPACK_IMPORTED_MODULE_1__["default"].mount();
  });
  document.addEventListener("shopify:section:unload", function () {
    _app__WEBPACK_IMPORTED_MODULE_1__["default"].unmount();
    _app__WEBPACK_IMPORTED_MODULE_1__["default"].mount();
  });
}

_app__WEBPACK_IMPORTED_MODULE_1__["default"].on("app:update", function () {
  _app__WEBPACK_IMPORTED_MODULE_1__["default"].unmount();
  _app__WEBPACK_IMPORTED_MODULE_1__["default"].mount();
});
_app__WEBPACK_IMPORTED_MODULE_1__["default"].mount();
}();
/******/ })()
;